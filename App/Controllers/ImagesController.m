//
//  ImagesController.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "ImagesController.h"
#import "ImageCell.h"

@implementation ImagesController

@synthesize imageTableView;
@synthesize images;
@synthesize images_urls;
@synthesize concurrently;
@synthesize mem_images;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [imageTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [imageTableView setBackgroundColor:[UIColor clearColor]];
    
    images_urls = [[NSMutableArray alloc] initWithArray:@[
            @"https://upload.wikimedia.org/wikipedia/commons/7/74/Earth_poster_large.jpg",
            @"http://kenrockwell.com/nikon/d600/sample-images/600_0985.JPG",
            @"http://www.austal.com/sites/default/files/media-images/DSC06071.JPG",
            @"http://www.marinelink.com/images/maritime/265a5884ef314b6aa70b00d28e986cabweb-33268.jpg"
            ]];
    mem_images = [[NSMutableArray alloc] init];
    images = [[NSMutableDictionary alloc] init];
}

- (NSString *)diskNamePrefix {
    return !concurrently?@"a":@"b";
}

- (void)loadImages {
    
    if (downloadStarted)
        return;
    
    inMemory = 0;
    downloaded = 0;
    mem_images = [[NSMutableArray alloc] init];
    images = [[NSMutableDictionary alloc] init];
    [imageTableView reloadData];
    
    downloadStarted = YES;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    for (NSString *url in images_urls) {
        [[[HttpHandler alloc] initWithDelegate:self] getImage:url diskNamePrefix:[self diskNamePrefix]];
    }
}

- (void)responseImageWithParams:(NSArray *)params {
    [self imageFinsied:params[0]];
}

- (void)responseWithError:(NSString *)error WithParams:(NSArray *)params {
    [self imageFinsied:params[0]];
}

- (void)response:(NSString *)response WithParams:(NSArray *)params {
    
}

- (void)imageFinsied:(NSString *)url {
    downloaded ++;
    
    if (!concurrently) {
        if (downloaded == images_urls.count) {
            [self performSelectorInBackground:@selector(getResizedImagesFromDisk) withObject:nil];
        }
    } else {
        [self performSelectorInBackground:@selector(getResizedImageFromDisk:) withObject:url];
    }
}

- (void)getResizedImagesFromDisk {
    for (NSString *url in images_urls) {
        [self getResizedImageFromDisk:url];
    }
    
    [self performSelectorOnMainThread:@selector(reloadTableData) withObject:nil waitUntilDone:YES];
}

- (void)getResizedImageFromDisk:(NSString *)url {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    
    CGFloat width = screenBounds.size.width * screenScale;
    
    NSString *diskName = [[self diskNamePrefix] stringByAppendingString:url];
    UIImage *image = [Common resizeImageToMaxSize:width withfile:diskName];
    
    if (!concurrently) {
        [images setValue:image forKey:url];
    } else {
        [mem_images addObject:image?image:[[UIImageView alloc] init]];
    }
    
    if (concurrently)
        [self performSelectorOnMainThread:@selector(reloadTableData) withObject:nil waitUntilDone:YES];
}

- (void)reloadTableData {
    if (!concurrently) {
        inMemory = downloaded;
        [imageTableView reloadData];
    } else {
        inMemory ++;
        NSIndexPath *ip = [NSIndexPath indexPathForRow:mem_images.count-1 inSection:0];
        [imageTableView insertRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationTop];
    }
    
    if (inMemory == images_urls.count) {
        downloadStarted = NO;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return inMemory;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageCell *cell = [imageTableView dequeueReusableCellWithIdentifier:@"ImageCell" forIndexPath:indexPath];
    
    UIImage *image;
    if (!concurrently) {
        NSString *url = [images_urls objectAtIndex:indexPath.row];
        image = [images valueForKey:url];
    } else
        image = [mem_images objectAtIndex:indexPath.row];
    
    [cell.imageview setImage:image];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

@end
