//
//  Common.h
//  Yummy Wok
//
//  Created by Devloper on 3/8/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HttpHandler.h"
#import "UserDetails.h"

@interface Common : NSObject

+ (id) valueForKey:(NSString *)key;
+ (id) valueForKey:(NSString *)key defaultValue:(id)default_value;
+ (void) setValue:(id)value forKey:(NSString *)key;
+ (void)saveObject:(NSObject *)object key:(NSString *)key;
+ (NSObject *)loadObjectWithKey:(NSString *)key;
+ (NSString *)getFullName:(UserDetails *)user;
+ (BOOL) validateEmail: (NSString *) candidate;
+ (NSString *)getStringForArray:(NSMutableArray *)array;
+ (NSString *)stringEscaping:(NSString *)unescapedString;
+ (BOOL)reachable;
+ (void)styleNavigationBar:(UINavigationController *)navigationController;
+ (NSString *)countryNameForCode:(NSString *)country_code;
+ (NSString *)languageNameForCode:(NSString *)lang_code;
+ (BOOL) validateUrl: (NSString *)candidate;
+ (NSTimeInterval)dateToTimeStamp:(NSString *)dateString;
+ (NSString *)timeStampToDate:(NSString *)timeStamp;
+ (NSString *)timeStampToDate:(NSString *)timeStamp withLocale:(NSString *)locale;
+ (NSString *)timeStampToDateTime:(NSString *)timeStamp withLocale:(NSString *)locale;
+ (NSString *)timeStampToLongDate:(NSString *)timeStamp withLocale:(NSString *)locale;
+ (NSDate *)stringToDate:(NSString *)dateString;
+ (NSString *)dateToString:(NSDate *)date;
+ (NSString *)dateToDateTimeString:(NSDate *)date;
+ (void)composeEmailWithCountriesList:(UIViewController *)launcher string:(NSString *)string;
+ (void)registerForPushNotification;
+ (NSString *)replaceFirstOccuarnceFromString: (NSString *)input withOriginal:(NSString *)original AndReplacment:(NSString *)replacement;
+ (NSString *)pathForName:(NSString *)name withExt:(NSString *)ext forScale:(NSInteger)scale;
+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;
+ (void)appendData:(NSData *)data toFile:(NSString *)file_name;
+ (UIImage *)resizeImageToMaxSize:(CGFloat)max withfile:(NSString*)file_name;
+ (void)fileExists:(NSString *)file_name;
+ (void)localNotificationWithText:(NSString *)text;

@end
