//
//  DBLayer.m
//  Watershade
//
//  Created by Hasan S. Al-Bukhari on 10/27/14.
//  Copyright (c) 2015 watershade. All rights reserved.
//

#import "DBLayer.h"
#import "Common.h"

@implementation DBLayer

#define DataBaseName @"for9a.db"

+ (DBLayer*)database {
    static DBLayer *_database = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _database = [[DBLayer alloc] init];
    });
    return _database;
}

- (id)init {
    if ((self = [super init])) {

        @try {
            
            [self createDatabase];
            
            NSURL *libraryDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSLibraryDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            NSString *sqLiteDb = [[libraryDirectoryURL URLByAppendingPathComponent:DataBaseName] path];
            
            if (sqlite3_open([sqLiteDb UTF8String], &_database) != SQLITE_OK) {
                DDLogVerbose(@"Failed to open database!");
                DDLogVerbose(@"Failed to open database at with error %s", sqlite3_errmsg(_database));
            }
        }
        @catch (NSException *exception) {
            DDLogVerbose(@"Couldn't open database.");
        }
    }
    return self;
}

- (void)createDatabase {
    NSString *libraryDirectoryURL;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    libraryDirectoryURL = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    NSString *databasePath = [[NSString alloc] initWithString: [libraryDirectoryURL stringByAppendingPathComponent:DataBaseName]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if (![filemgr fileExistsAtPath:databasePath]) {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &_database) == SQLITE_OK) {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS Locations (latitude TEXT, longitude TEXT, Altitude TEXT);";
            
            if (sqlite3_exec(_database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                DDLogVerbose(@"Failed to create table");
            }
            sqlite3_close(_database);//////////
        }
        else {
            DDLogVerbose(@"Failed to open/create database");
        }
    }
}

- (BOOL)insertLocation:(double)latitude and:(double)longtude and:(double)altitude {
    BOOL success = NO;
    
    NSString *base_query = @"INSERT INTO Locations(latitude, longitude, Altitude) VALUES(?, ?, ?)";
    NSString *query = [[NSString alloc] initWithString:base_query];
    success = NO;
    
    sqlite3_stmt *statement;
            
    if(sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        sqlite3_bind_text(statement, 1, [[NSString stringWithFormat:@"%f", latitude] UTF8String], -1, nil);
        sqlite3_bind_text(statement, 2, [[NSString stringWithFormat:@"%f", longtude] UTF8String], -1, nil);
        sqlite3_bind_text(statement, 3, [[NSString stringWithFormat:@"%f", altitude] UTF8String], -1, nil);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
            success = YES;
        sqlite3_finalize(statement);
    }
    
    return success;
}

@end
