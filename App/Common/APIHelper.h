//
//  APIHelper.h
//  Souq Price Tracker
//
//  Created by Hasan S. Al-Bukhari on 2/26/16.
//  Copyright © 2016 Hasan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol APIAuthorizationDelegate <NSObject>

@required
- (void)userAuthorized:(NSString *)code;
@end

@interface APIHelper : NSObject
{
    NSString *_baseURL;
    NSString *_apiKey;
    NSString *_authenticationToken;
    
    __weak id <APIAuthorizationDelegate> _delegate;
}

@property (nonatomic, strong) NSDictionary *plistDictionary;

@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, strong) NSString *authenticationToken;

@property (nonatomic, weak) id <APIAuthorizationDelegate> delegate;

- (id)uriForKey:(NSString *)method_name;
- (id)valueForKey:(NSString *)key;

@end
