//
//  ActivityParser.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityParser : NSObject

@property (nonatomic, strong) NSMutableArray *activities;

- (id)initWithJSON:(NSString *)json;

@end
