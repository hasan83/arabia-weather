//
//  UserParser.h
//  Yummy Wok
//
//  Created by Devloper on 3/24/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDetails.h"

@interface UserParser : NSObject

@property (nonatomic, strong) UserDetails *user;
@property (nonatomic, strong) NSString *msg;

- (id)initWithJSON:(NSString *)json;

@end
