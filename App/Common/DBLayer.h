//
//  DBLayer.h
//  Watershade
//
//  Created by Hasan S. Al-Bukhari on 10/27/14.
//  Copyright (c) 2015 Watershade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBLayer : NSObject {
    sqlite3 *_database;
}

+ (DBLayer*)database;

- (BOOL)insertLocation:(double)latitude and:(double)longtude and:(double)altitude;


@end
