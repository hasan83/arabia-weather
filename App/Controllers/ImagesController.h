//
//  ImagesController.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "MasterController.h"
#import "HttpHandler.h"

@interface ImagesController : MasterController <UITableViewDelegate, UITableViewDataSource, HttpDelegate>
{
    NSInteger downloaded, inMemory, downloadStarted;
}

@property (weak, nonatomic) IBOutlet UITableView *imageTableView;

@property (strong, nonatomic) NSMutableArray *images_urls;
@property (strong, nonatomic) NSMutableDictionary *images;
@property (strong, nonatomic) NSMutableArray *mem_images;
@property (assign, nonatomic) BOOL concurrently;

- (void)loadImages;

@end
