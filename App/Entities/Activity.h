//
//  Activity.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Activity : NSObject

@property (nonatomic, strong) NSString *activity_color;
@property (nonatomic, strong) NSString *activity_dayInfo;
@property (nonatomic, strong) NSString *activity_dayInfo_en;
@property (nonatomic, strong) NSString *activity_status;
@property (nonatomic, strong) NSString *activity_status_en;
@property (nonatomic, strong) NSString *activity_text;
@property (nonatomic, strong) NSString *activity_text_en;
@property (nonatomic, strong) NSString *activity_type;

@end
