//
//  Images2Controller.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/15/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "Images2Controller.h"

@implementation Images2Controller

@synthesize controller;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tabBarController.title = [self.languageDetails LocalString:@"Images2"];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:[self.languageDetails LocalString:@"Go"]
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(GoButtonTapped:)];
    self.tabBarController.navigationItem.rightBarButtonItem = rightButton;
}

- (void)GoButtonTapped:(UIButton*)sender
{
    [controller loadImages];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    controller = segue.destinationViewController;
    controller.concurrently = YES;
}

@end
