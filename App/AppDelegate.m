//
//  AppDelegate.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "AppDelegate.h"
#import <TWMessageBarManager.h>

@implementation AppDelegate

@synthesize singlton;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Initialize the singlton object for the first and last time.
    singlton = [Singlton singlton];
    
    // Testing logging which has been configured in the singlton initialization.
    DDLogVerbose(@"Logging testing ...");
    DDLogVerbose(@"Name: %@", [[singlton userDetails] first_name]);
    
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        [application registerUserNotificationSettings:[UIUserNotificationSettings
                                                       settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|
                                                       UIUserNotificationTypeSound categories:nil]];
    }
    
    // check if notification received while app. closed.
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification)
        [self application:application didReceiveLocalNotification:notification];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if ([[Common valueForKey:@"Local" defaultValue:@"0"] isEqualToString:@"0"]) {
        [Common localNotificationWithText:[singlton.languageDetails LocalString:@"CheckTheWeather"]];
        [Common setValue:@"1" forKey:@"Local"];
    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    DDLogVerbose(@"Error in push notification registration. Error: %@", err);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"Notification Message: %@", notification.alertBody);
    
    UITabBarController *tabbarController = [self getTabbarController];
    if (!tabbarController) {
        [Common setValue:@"1" forKey:@"OpenController2"];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Notification Message: %@", [userInfo description]);
    
    UIApplicationState state = [application applicationState];
    NSLog(@"Application Status: %ld", (long)state);
    
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    NSDictionary *msg = [userInfo valueForKey:@"msg"];
    
    NSString *alert = [aps valueForKey:@"alert"];
}

- (UITabBarController *)getTabbarController {
    UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
    if (navController) {
        UITabBarController *tabbarController = (UITabBarController *)[navController.viewControllers firstObject];
        [tabbarController setSelectedIndex:1];
        return tabbarController;
    }
    return nil;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
