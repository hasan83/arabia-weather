//
//  ViewController.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterController.h"
#import <CoreLocation/CoreLocation.h>
#import "HttpHandler.h"
#import "LoadingView.h"

@interface ViewController : MasterController <CLLocationManagerDelegate, CLLocationManagerDelegate, HttpDelegate, LoadingDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) HttpHandler *httpHandler;
@property (strong, nonatomic) NSMutableArray *activities;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UITableView *activitiesTableView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *lastLocation;

@property (weak, nonatomic) IBOutlet UIView *noteView;
@property (weak, nonatomic) IBOutlet LoadingView *loadingView;

@end
