//
//  LanguageDetails.m
//  Yummy Wok
//
//  Created by Devloper on 3/8/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import "LanguageDetails.h"
#import "Common.h"

#import "Singlton.h"

@implementation LanguageDetails

@synthesize language;

- (id)init
{
    if (self = [super init]) {
        
        // user preferred languages. this the default app. language. first launch app. language!
        language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    }
    return  self;
}

// returns app. language direction.
- (BOOL)rtl {
    return [NSLocale characterDirectionForLanguage:language] == NSLocaleLanguageDirectionRightToLeft;
}

// switches language.
- (void)changeLanguage
{
    NSString *changeTo;
    // check current language to switch to the other.
    if ([language isEqualToString:@"ar"])
        changeTo = @"en";
    else
        changeTo = @"ar";
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:changeTo, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    DDLogVerbose(@"Language changed to: %@", changeTo);
}

// change language to a specfic one.
- (void)changeLanguageTo:(NSString *)lang
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:lang, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    DDLogVerbose(@"Language changed to: %@", lang);
}

- (NSLocale *)getLocale {
    if ([self rtl])
        return [[NSLocale alloc] initWithLocaleIdentifier:@"ar_JO"];
    else
        return [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
}

// get localized string based on app. langauge.
- (NSString *)LocalString:(NSString *)key {
    NSString *localizedString = NSLocalizedString(key, nil);
//    NSString *localizedString = NSLocalizedStringFromTableInBundle(key, [language isEqualToString:@"en"]?@"Base":language, [NSBundle mainBundle], nil);
    DDLogVerbose(@"Localized string '%@' for key '%@'", localizedString, key);
    return localizedString;
}

- (NSString *)LocalString:(NSString *)key forLang:(NSString *)lan {
    NSBundle *bundl = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[lan isEqualToString:@"ar"]?lan:@"Base" ofType:@"lproj"]];
    return NSLocalizedStringFromTableInBundle(key, @"Localizable", bundl, nil);
}

@end
