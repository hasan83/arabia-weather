//
//  APIHelper.m
//  Souq Price Tracker
//
//  Created by Hasan S. Al-Bukhari on 2/26/16.
//  Copyright © 2016 Hasan. All rights reserved.
//

#import "APIHelper.h"

@implementation APIHelper

@synthesize baseURL = _baseURL;
@synthesize apiKey = _apiKey;
@synthesize authenticationToken = _authenticationToken;

@synthesize plistDictionary;

- (id)init
{
    if (self = [super init]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"api_config" ofType:@"plist"];
        if (path) {
            plistDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
        }
    }
    return  self;
}

- (NSString *)baseURL {
    if (!_baseURL) {
#ifdef DEBUG
        _baseURL = [self valueForKey:PListDevBaseUrlKey];
#else
        _baseURL = [self valueForKey:PListBaseUrlKey];
#endif
    }
    return _baseURL;
}

- (NSString *)apiKey {
    if (!_apiKey) {
        _apiKey = [self valueForKey:PListAPIKeyKey];
    }
    return _apiKey;
}

- (id) uriForKey:(NSString *)method_name {
    return [self valueForKey:method_name];
}

- (id) valueForKey:(NSString *)key {
    return [plistDictionary valueForKey:key];
}

- (NSString *)authenticationToken {
    if (!_authenticationToken || [_authenticationToken isEqualToString:@""]) {
        _authenticationToken = [Common valueForKey:AuthenticationTokenUserDefaultsKey defaultValue:nil];
    }
    return _authenticationToken;
}

- (void)setAuthenticationToken:(NSString *)authenticationToken {
    _authenticationToken = authenticationToken;
    [Common setValue:authenticationToken forKey:AuthenticationTokenUserDefaultsKey];
}

@end
