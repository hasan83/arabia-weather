//
//  AppDelegate.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singlton.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) Singlton *singlton;

@end

