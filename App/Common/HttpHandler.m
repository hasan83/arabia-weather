//
//  HttpHandler.m
//  Yummy Wok
//
//  Created by Hasan S. Al-Bukhari on 12/11/14.
//  Copyright (c) 2014 Watershade. All rights reserved.
//

#import "HttpHandler.h"
#import "Singlton.h"

@implementation HttpHandler

@synthesize delegate = _delegate;
@synthesize baseUrl = _baseUrl;
@synthesize api_key = _api_key;
@synthesize user_name = _user_name;
@synthesize password = _password;
@synthesize received_data = _received_data;
@synthesize download_connection = _download_connection;
@synthesize params = _params;
@synthesize extras = _extras;
@synthesize language = _language;
@synthesize singlton;

// only to initialise object and delegate. no server hits.
- (id)initWithDelegate:(id <HttpDelegate>)del
{
    if ([super init]) {
        
        singlton = [Singlton singlton];
        
        self.delegate = del;
        self.baseUrl = [[singlton apiHelper] baseURL];
        self.api_key = [[singlton apiHelper] apiKey];
        self.user_name = @"";
        self.password = @"";
        self.language = [[singlton languageDetails] language];
    }
    return self;
}

- (void)callEntity:(NSString *)entity_name method:(NSString *)method_name withParams:(NSArray *)params andValues:(NSArray *)values andExtras:(NSArray *)extras {
    [self callEntity:entity_name method:method_name withParams:params andValues:values andExtras:extras withRequestMethod:@"POST"];
}

// call a service method.
- (void)callEntity:(NSString *)entity_name method:(NSString *)method_name withParams:(NSArray *)params andValues:(NSArray *)values andExtras:(NSArray *)extras withRequestMethod:(NSString *)request_method {
    self.extras = extras;
    self.params = @[entity_name, method_name, params, values, extras];
    
    // initialise the method url.
    NSString *url = [NSString stringWithFormat:@"%@/%@/%@",
                     [self baseUrl], [[singlton apiHelper] uriForKey:entity_name], [[singlton apiHelper] uriForKey:method_name]];
    
    url = [url stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
    url = [url stringByReplacingOccurrencesOfString:@":/" withString:@"://"];
    if ([[url substringFromIndex:url.length-1] isEqualToString:@"/"])
        url = [url substringToIndex:url.length-1];
    url = [url stringByAppendingString:@"?prevent_cash_random="];
    url = [url stringByAppendingFormat:@"%d", arc4random_uniform(74)];
    
    // create post body
    NSString *contentType = [[singlton apiHelper] valueForKey:PListAPIContentTypeKey];
    NSString *accept = [[singlton apiHelper] valueForKey:PListAPIAcceptKey];
    NSString *authenticationToken = [[singlton apiHelper] authenticationToken];
    NSString *httpBody = @"";
    NSString *seperator = @"";
    
    // add parameters and values
    for (int i=0;i<params.count;i++)
    {
        if ([params[i] isEqualToString:@""]) {
            url = [Common replaceFirstOccuarnceFromString:url withOriginal:@"%@" AndReplacment:values[i]];
        } else {
            if ([request_method isEqualToString:@"POST"] || [request_method isEqualToString:@"PUT"]) {
                httpBody = [httpBody stringByAppendingString:[seperator stringByAppendingString: [NSString stringWithFormat:@"%@=%@", [[singlton apiHelper] valueForKey:[params objectAtIndex:i]], [values objectAtIndex:i]]]];
                seperator = @"&";
            } else {
                seperator = @"&";
                url = [url stringByAppendingString:[seperator stringByAppendingString: [NSString stringWithFormat:@"%@=%@", [[singlton apiHelper] valueForKey:[params objectAtIndex:i]], [values objectAtIndex:i]]]];
            }
        }
    }
    NSData *bodyData = [httpBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *URL = [[NSURL alloc] initWithString:url];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:connection_time_out];
    
    [request setHTTPMethod:request_method];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setValue:accept forHTTPHeaderField:@"Accept"];
    [request setValue:authenticationToken forHTTPHeaderField:[[singlton apiHelper] valueForKey:PListAuthenticationTokenKey]];
//    [request setValue:@"en" forHTTPHeaderField:@"Lang"];
//    [request setValue:self.user_name forHTTPHeaderField:@"X_USERNAME"];
//    [request setValue:self.password forHTTPHeaderField:@"X_PASSWORD"];
    if ([request_method isEqualToString:@"POST"] || [request_method isEqualToString:@"PUT"])
        [request setHTTPBody:bodyData];
    
    self.received_data = [[NSMutableData alloc] init];
    
    // if the connection running cancel it. to init a new one.
    if (self.download_connection != nil) {
        [self.download_connection cancel];
        self.download_connection = nil;
    }
    // start the http hit
    self.download_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    // logging curl command! nice ha :)
    DDLogVerbose(@"Curl Command: curl -X %@ --header \"Content-Type: %@\" --header \"Accept: %@\" --header \"API-Key: %@\" --header \"Authentication: %@\" --header \"Lang: %@\" -d \"%@\" \"%@\"", [request HTTPMethod], contentType, accept, self.api_key, authenticationToken, self.language, [httpBody stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""], url);
}

// call a service method.
- (void)getImage:(NSString *)url_string diskNamePrefix:(NSString *)disk_name_prefix {
    [Common fileExists:[disk_name_prefix stringByAppendingString:url_string]];
    
    isImage = YES;
    self.params = @[url_string, disk_name_prefix];
    
    NSURL *URL = [[NSURL alloc] initWithString:url_string];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:connection_time_out];
    
    self.received_data = [[NSMutableData alloc] init];
    
    // if the connection running cancel it. to init a new one.
    if (self.download_connection != nil) {
        [self.download_connection cancel];
        self.download_connection = nil;
    }
    
    // start the http hit
    self.download_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    DDLogVerbose(@"Curl Command: curl %@", url_string);
}

// call a service method.
- (void)callEntity2:(NSString *)entity_name method:(NSString *)method_name withParams:(NSArray *)params andValues:(NSArray *)values andExtras:(NSArray *)extras {
    self.extras = extras;
    self.params = @[entity_name, method_name, params, values, extras];
    
    // initialise the method url.
    NSString *url = [NSString stringWithFormat:@"%@/%@/%@",
                     [self baseUrl], [[singlton apiHelper] uriForKey:entity_name], [[singlton apiHelper] uriForKey:method_name]];
    
    url = [url stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
    url = [url stringByReplacingOccurrencesOfString:@":/" withString:@"://"];
    if ([[url substringFromIndex:url.length-1] isEqualToString:@"/"])
        url = [url substringToIndex:url.length-1];
    
    // create post body
    NSString *boundary = @"3290x490m804903284nc09n8pasid";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSString *accept = [[singlton apiHelper] valueForKey:PListAPIAcceptKey];
    NSString *authenticationToken = [[singlton apiHelper] authenticationToken];
    NSMutableData *bodyData = [NSMutableData data];
    
    for (int i=0;i<params.count-1;i++) {
        [bodyData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", [[singlton apiHelper] valueForKey:[params objectAtIndex:i]]] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"%@\r\n", [values objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSData *imageData = [values objectAtIndex:values.count-1];
    if (imageData) {
        [bodyData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", [[singlton apiHelper] valueForKey:[params objectAtIndex:params.count-1]]] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:imageData];
        [bodyData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [bodyData appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURL *URL = [[NSURL alloc] initWithString:url];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:connection_time_out];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setValue:accept forHTTPHeaderField:@"Accept"];
    [request setValue:authenticationToken forHTTPHeaderField:[[singlton apiHelper] valueForKey:PListAuthenticationTokenKey]];
    //    [request setValue:self.user_name forHTTPHeaderField:@"X_USERNAME"];
    //    [request setValue:self.password forHTTPHeaderField:@"X_PASSWORD"];
    [request setHTTPBody:bodyData];
    
    self.received_data = [[NSMutableData alloc] init];
    
    // if the connection running cancel it. to init a new one.
    if (self.download_connection != nil) {
        [self.download_connection cancel];
        self.download_connection = nil;
    }
    // start the http hit
    self.download_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

// cancel the http hit. It's not garanteed that the data didn't received to the server. you will not get the response though.
- (void)cancel
{
    self.delegate = nil;
    if (self.download_connection != nil) {
        [self.download_connection cancel];
        self.download_connection = nil;
    }
}

// will be called if a password set to the services folder
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:self.user_name password:self.password persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    } else {
        DDLogVerbose(ConnectionAuthenticationFailure);
        if (self.delegate)
            [self.delegate responseWithError:ConnectionAuthenticationFailure WithParams:self.params];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.download_connection cancel];
    self.download_connection = nil;
    
    if (self.delegate)
        [self.delegate responseWithError:error.description WithParams:self.params];
    DDLogVerbose(@"Connection Failed with error: %@", [error description]);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    NSInteger state = [httpResponse statusCode];
        
    if (state >= 400 && state <600)
    {
        // something wrong happen.
        [self.download_connection cancel];
        self.download_connection = nil;
        
        if (self.delegate)
            [self.delegate responseWithError:[response description] WithParams:self.params];
    }
    
    if ([[[httpResponse allHeaderFields] allKeys] containsObject:[[singlton apiHelper] valueForKey:PListAuthenticationTokenKey]]) {
        NSString *authenticationToken = [[httpResponse allHeaderFields] valueForKey:[[singlton apiHelper] valueForKey:PListAuthenticationTokenKey]];
        if (authenticationToken && ![authenticationToken isEqual:[NSNull null]] && ![authenticationToken isEqualToString:@""])
            [[singlton apiHelper] setAuthenticationToken:authenticationToken];
        DDLogVerbose(@"HTTP Response Authentication Token : %@", authenticationToken);
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.received_data appendData:data];
    
    if (isImage && self.received_data.length > 1000000) {
        [Common appendData:self.received_data toFile:[self.params[1] stringByAppendingString:self.params[0]]];
        self.received_data = [[NSMutableData alloc] init];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (!isImage) {
        NSString *decoded_string = [[NSString alloc] initWithBytes:[self.received_data bytes] length:[self.received_data length] encoding:NSUTF8StringEncoding];
        DDLogVerbose(@"JSON : %@", decoded_string);
    
        if (self.delegate) {
            [self.delegate response:decoded_string WithParams:self.params];
        }
    } else {
        if (self.received_data.length > 0) {
            [Common appendData:self.received_data toFile:[self.params[1] stringByAppendingString:self.params[0]]];
        }
        
        if (self.delegate) {
            [self.delegate responseImageWithParams:self.params];
        }
    }
    
    self.download_connection = nil;
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

@end
