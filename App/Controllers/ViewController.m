//
//  ViewController.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "ViewController.h"
#import "Activity.h"
#import "ActivityParser.h"
#import "ActivityCell.h"
#import "DBLayer.h"

@implementation ViewController

@synthesize locationManager;
@synthesize lastLocation;
@synthesize noteView;
@synthesize httpHandler;
@synthesize loadingView;
@synthesize activities;
@synthesize activitiesTableView;
@synthesize refreshControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [activitiesTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [activitiesTableView setBackgroundColor:[UIColor clearColor]];
    [activitiesTableView setRowHeight:UITableViewAutomaticDimension];
    [activitiesTableView setEstimatedRowHeight:68.0];
    [activitiesTableView registerNib:[UINib nibWithNibName:@"ActivityCell" bundle:nil] forCellReuseIdentifier:@"ActivityCell"];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [refreshControl setTintColor:[self.stylingDetails themeColor]];
    [activitiesTableView addSubview:refreshControl];
    
    [loadingView setDelegate:self];
    
    activities = [[NSMutableArray alloc] init];
    NSString *cached_json = [Common valueForKey:CacheKey defaultValue:@""];
    ActivityParser *parser = [[ActivityParser alloc] initWithJSON:cached_json];
    if (parser.activities && parser.activities > 0) {
        activities = parser.activities;
    }

    [noteView setHidden:YES];
    
    [self loadLifestyle];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getLocation];
    
    self.tabBarController.title = [self.languageDetails LocalString:@"Lifestyle"];
    
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[Common valueForKey:@"OpenController2" defaultValue:@"0"] isEqualToString:@"1"]) {
        [Common setValue:@"0" forKey:@"OpenController2"];
        [self.tabBarController setSelectedIndex:1];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [activitiesTableView setLayoutMargins:UIEdgeInsetsZero];
}

- (void)loadLifestyle {
    
    if (httpHandler) {
        [httpHandler cancel];
    }
    
    [(httpHandler = [[HttpHandler alloc] initWithDelegate:self]) callEntity:PListLifestyleEntityKey method:PListLifestyleMethodKey withParams:@[] andValues:@[] andExtras:@[] withRequestMethod:@"GET"];
    
    if (activities.count > 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    } else {
        [loadingView startLoadingWitMessage:@"LifestyleLoading"];
    }
}

- (void)reload {
    [self loadLifestyle];
}

- (void)refresh:(UIRefreshControl *)refresh {
    [self loadLifestyle];
}

- (void)response:(NSString *)response WithParams:(NSArray *)params {
    [refreshControl endRefreshing];
    [Common setValue:response forKey:CacheKey];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    ActivityParser *parser = [[ActivityParser alloc] initWithJSON:response];
    if (!parser.activities) {
        if (activities.count > 0) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Lifestyle", nil) message:NSLocalizedString(@"FailTryAgain", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        } else {
            [loadingView stopLoadingWithError:@"FailTryAgain"];
        }
    } else if (parser.activities.count == 0) {
        if (activities.count > 0) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Lifestyle", nil) message:NSLocalizedString(@"NoMore", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        } else {
            [loadingView stopLoadingWithError:@"NoMore"];
        }
    } else {
        [loadingView stopLoading];
        activities = parser.activities;
        [activitiesTableView reloadData];
    }
}

- (void)responseWithError:(NSString *)error WithParams:(NSArray *)params {
    [refreshControl endRefreshing];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (activities.count > 0) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Lifestyle", nil) message:NSLocalizedString(@"ServerUnreachable", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        [loadingView stopLoading];
    } else {
        [loadingView stopLoadingWithError:@"ServerUnreachable"];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return activities.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSMutableArray *)[activities objectAtIndex:section]).count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSMutableArray *acts = [activities objectAtIndex:section];
    if (acts.count > 0) {
        Activity *activity = [acts objectAtIndex:0];
        return [self.languageDetails.language isEqualToString:@"ar"] ? activity.activity_dayInfo : activity.activity_dayInfo_en;
    }
    
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    ActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityCell" forIndexPath:indexPath];
    
    // clear selection on selected cells.
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setSelectedBackgroundView:[[UIView alloc] init]];
    [cell.selectedBackgroundView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundView:[[UIView alloc] init]];
    [cell.backgroundView setBackgroundColor:[UIColor clearColor]];
 
    Activity *activity = [[activities objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell setActivity:activity];
    
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (void)getLocation
{
    if (locationManager == nil)
    {
        [self initLocationManager];
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestWhenInUseAuthorization];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager startUpdatingLocation];
    } else {
        [noteView setHidden:NO];
    }
}

- (void)initLocationManager {
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = 50.0;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    DDLogVerbose(@"location manager error: %@", error.description);
}

- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            DDLogVerbose(@"User still thinking granting location access!");
            [locationManager stopUpdatingLocation];
        } break;
        case kCLAuthorizationStatusDenied: {
            DDLogVerbose(@"User denied location access request!!");
            [noteView setHidden:NO];
            [locationManager stopUpdatingLocation];
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [noteView setHidden:YES];
            [locationManager startUpdatingLocation]; // Will update location immediately
        } break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    lastLocation = [locations lastObject];
    bool haveValidAltitude = (lastLocation.verticalAccuracy > 0);
    CLLocationDistance altitude = 0;
    if(haveValidAltitude)
        altitude = lastLocation.altitude;
    DDLogVerbose(@"last location: lat %f - lon % - alt %f", lastLocation.coordinate.latitude, lastLocation.coordinate.longitude, altitude);
    
    if ([[DBLayer database] insertLocation:lastLocation.coordinate.latitude and:lastLocation.coordinate.longitude and:altitude])
        DDLogVerbose (@"DB Success");
    else
        DDLogVerbose (@"DB Fail");
}

@end
