//
//  UserDetails.h
//  Yummy Wok
//
//  Created by Devloper on 3/8/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetails : NSObject
{
    BOOL no_cache;
}

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *product_types;
@property (nonatomic, strong) NSString *access_token;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *full_name;
@property (nonatomic, strong) NSString *employee_code;
@property (nonatomic, strong) NSString *date_of_birth;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *department_id;
@property (nonatomic, strong) NSString *position_id;
@property (nonatomic, strong) NSString *direct_manager;
@property (nonatomic, strong) NSString *last_login;
@property (nonatomic, strong) NSString *department_name;
@property (nonatomic, strong) NSString *position_name;
@property (nonatomic, strong) NSString *direct_manager_full_name;
@property (nonatomic, strong) NSString *profile_pic;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *provider;
@property (nonatomic, strong) NSString *provider_id;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *age;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *facebook_url;
@property (nonatomic, strong) NSString *twitter_url;
@property (nonatomic, strong) NSString *linkedin_url;
@property (nonatomic, strong) NSString *google_url;
@property (nonatomic, strong) NSString *resume;
@property (nonatomic, strong) NSMutableArray *languages;
@property (nonatomic, strong) NSString *show_steps;
@property (nonatomic, strong) NSMutableArray *opportunity_types;
@property (nonatomic, strong) NSMutableArray *opportunity_specialties;
@property (nonatomic, strong) NSMutableArray *languages_to_learn;
@property (nonatomic, strong) NSString *completeness_percentage;
@property (nonatomic, strong) NSString *opportunities_applied_count;
@property (nonatomic, strong) NSString *opportunities_pinned_count;
@property (nonatomic, strong) NSString *organizations_followed_count;
@property (nonatomic, strong) NSString *suitable_opportunities;

@property (nonatomic, strong) NSString *first_name;
@property (nonatomic, strong) NSString *last_name;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *device_token;

- (id)initWithNoCache;
- (void)setNoCache:(BOOL)flag;

- (BOOL)userLoggedIn;
- (void)setUser:(UserDetails *)user;
- (void)setUserProfile:(UserDetails *)user;
- (BOOL)isFacebookUser;
- (BOOL)isLinkedInUser;
- (BOOL)isSocialLogin;
- (BOOL)isShow_steps;
- (BOOL)isOrganization;

@end
