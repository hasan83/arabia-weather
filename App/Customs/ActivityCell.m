//
//  ActivityCell.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "ActivityCell.h"
#import "Singlton.h"

@implementation ActivityCell

@synthesize txtLabel;
@synthesize statusLabel;
@synthesize activity;

- (void)setActivity:(Activity *)object {
    activity = object;
    
    if ([((Singlton *)[Singlton singlton]).languageDetails.language isEqualToString:@"ar"]) {
        txtLabel.text = activity.activity_text;
        statusLabel.text = activity.activity_status;
    } else {
        txtLabel.text = activity.activity_text_en;
        statusLabel.text = activity.activity_status_en;
    }
    
    [self.contentView setBackgroundColor:[Common colorWithHexString:[activity.activity_color uppercaseString]]];
}

@end
