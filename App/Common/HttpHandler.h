//
//  HttpHandler.h
//  Yummy Wok
//
//  Created by Hasan S. Al-Bukhari on 12/11/14.
//  Copyright (c) 2014 Watershade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singlton.h"

@protocol HttpDelegate <NSObject>

@required
- (void)response:(NSString *)response WithParams:(NSArray *)params;
- (void)responseWithError:(NSString *)error WithParams:(NSArray *)params;
- (void)responseImageWithParams:(NSArray *)params;
@end

@interface HttpHandler : NSObject <NSURLConnectionDataDelegate>
{
    __weak id <HttpDelegate> _delegate;
    NSString *_baseUrl; // base url for services
    NSString *_api_key; // api_key for services
    NSString *_user_name; // services folder username
    NSString *_password; // services folder password
    NSMutableData *_received_data;
    NSURLConnection *_download_connection;
    NSArray *_params; // post or get params
    NSArray *_extras; // extras to be sent back to the delegate
    NSString *_language; // to get fetched data in this specified language.
    BOOL isImage;
}

- (id)initWithDelegate:(id <HttpDelegate>)del;
- (void)callEntity:(NSString *)entity_name method:(NSString *)method_name withParams:(NSArray *)params andValues:(NSArray *)values andExtras:(NSArray *)extras;
- (void)callEntity:(NSString *)entity_name method:(NSString *)method_name withParams:(NSArray *)params andValues:(NSArray *)values andExtras:(NSArray *)extras withRequestMethod:(NSString *)request_method;
- (void)callEntity2:(NSString *)entity_name method:(NSString *)method_name withParams:(NSArray *)params andValues:(NSArray *)values andExtras:(NSArray *)extras;
- (void)getImage:(NSString *)url_string diskNamePrefix:(NSString *)disk_name_prefix;
- (void)cancel;

@property (nonatomic,weak) id <HttpDelegate> delegate;
@property (nonatomic,strong) NSString *baseUrl;
@property (nonatomic,strong) NSString *api_key;
@property (nonatomic,strong) NSString *user_name;
@property (nonatomic,strong) NSString *password;
@property (nonatomic,strong) NSString *language;
@property (nonatomic,strong) NSMutableData *received_data;
@property (nonatomic,strong) NSURLConnection *download_connection;
@property (nonatomic,strong) NSArray *params;
@property (nonatomic,strong) NSArray *extras;

@property (nonatomic, weak) Singlton *singlton;

@end
