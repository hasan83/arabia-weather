//
//  Common.m
//  Yummy Wok
//
//  Created by Devloper on 3/8/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import "Common.h"
#import "Singlton.h"

#import <MessageUI/MessageUI.h>
#import <objc/runtime.h>
#import <ImageIO/ImageIO.h>

@implementation Common

// get NSUserDefaults value for key
+ (id) valueForKey:(NSString *)key
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    id val = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return val;
}

// get NSUserDefaults value for key and set default value if not exists
+ (id) valueForKey:(NSString *)key defaultValue:(id)default_value
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    id val = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (val)
        return val;
    [Common setValue:default_value forKey:key];
    return default_value;
}

// set value for key in NSUserDefaults
+ (void) setValue:(id)value forKey:(NSString *)key
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// should be remove I think!
+ (void)saveObject:(NSObject *)object key:(NSString *)key
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

// should be remove I think!
+ (NSObject *)loadObjectWithKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

// get full name
+ (NSString *)getFullName:(UserDetails *)user {
    // setting name
    NSString *full_name = [user full_name];
    if (!full_name || [full_name isEqualToString:@""])
        full_name = [user.first_name stringByAppendingFormat:@" %@", user.last_name];
    return full_name;
}

// validates email
+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL) validateUrl: (NSString *) candidate {
    NSURL *candidateURL = [NSURL URLWithString:candidate];
    // WARNING > "test" is an URL according to RFCs, being just a path
    // so you still should check scheme and all other NSURL attributes you need
    if (candidateURL && candidateURL.scheme && candidateURL.host) {
        // candidate is a well-formed url with:
        //  - a scheme (like http://)
        //  - a host (like stackoverflow.com)
        return YES;
    }
    return NO;
}

+ (NSString *)getStringForArray:(NSMutableArray *)array {
    NSString *string = @"", *delimiter = @"";
    for (NSString *value in array) {
        string = [string stringByAppendingFormat:@"%@%@", delimiter, value];
        delimiter = @",";
    }
    return string;
}

+ (NSString *)stringEscaping:(NSString *)unescapedString {
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)unescapedString,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
    return encodedString;
}

+ (void)styleNavigationBar:(UINavigationController *)navigationController {
    // navigation bar color
    navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    navigationController.navigationBar.barTintColor = [((Singlton *)[Singlton singlton]).stylingDetails themeColor];
    navigationController.navigationBar.translucent = NO;
    
    // dark navigation bar style forces light status bar
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    // font
    [navigationController.navigationBar setTitleTextAttributes:
         @{NSFontAttributeName:[UIFont fontWithName:FontRabbit size:16]}];
    NSDictionary *barButtonAppearanceDict = @{NSFontAttributeName : [UIFont fontWithName:FontRabbit size:14.0], NSForegroundColorAttributeName: [UIColor whiteColor]};
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
}

+ (NSString *)countryNameForCode:(NSString *)country_code {
    NSString *identifier = [NSLocale localeIdentifierFromComponents:[NSDictionary dictionaryWithObject:country_code forKey:NSLocaleCountryCode]];
    NSString *country = [[NSLocale currentLocale] displayNameForKey:NSLocaleIdentifier value:identifier];
    if ([[country lowercaseString] containsString:@"palestine"])
        country = @"Palestine";
    if ([[country lowercaseString] containsString:@"الفلسطين"])
        country = @"فلسطين";
    return country ? country : @"";
}

+ (NSString *)languageNameForCode:(NSString *)lang_code {
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:lang_code];
    NSString *userLanguage = ((Singlton *)[Singlton singlton]).languageDetails.language;
    return [locale displayNameForKey:NSLocaleIdentifier value:userLanguage];
}

+ (NSTimeInterval)dateToTimeStamp:(NSString *)dateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [formatter dateFromString:dateString];
    return [date timeIntervalSince1970];
}

+ (NSString *)timeStampToDate:(NSString *)timeStamp {
    return [Common timeStampToDate:timeStamp withLocale:@"en"];
}

+ (NSString *)timeStampToDate:(NSString *)timeStamp withLocale:(NSString *)locale {
    NSTimeInterval ts = [timeStamp integerValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:locale]];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:ts];
    NSString *stringDate = [formatter stringFromDate:date];
    return stringDate;
}

+ (NSString *)timeStampToLongDate:(NSString *)timeStamp withLocale:(NSString *)locale {
    NSTimeInterval ts = [timeStamp integerValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    if ([locale isEqualToString:@"ar"]) {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar_JO"]];
        [formatter setDateFormat:@"dd MMMM yyyy"];
    } else {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:locale]];
        [formatter setDateFormat:@"dd MMM. yyyy"];
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:ts];
    NSString *stringDate = [formatter stringFromDate:date];
    return stringDate;
}

+ (NSString *)timeStampToDateTime:(NSString *)timeStamp withLocale:(NSString *)locale {
    NSTimeInterval ts = [timeStamp integerValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:locale]];
    [formatter setDateFormat:@"dd-MM-yyyy hh:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:ts];
    NSString *stringDate = [formatter stringFromDate:date];
    return stringDate;
}

+ (NSDate *)stringToDate:(NSString *)dateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    NSDate *date = [formatter dateFromString:dateString];
    return date;
}

+ (NSString *)dateToString:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    return [formatter stringFromDate:date];
}

+ (NSString *)dateToDateTimeString:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy hh:mm"];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    return [formatter stringFromDate:date];
}

+ (void)composeEmailWithCountriesList:(UIViewController *)launcher string:(NSString *)string
{
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = launcher;
        
        [mailViewController setSubject:@"iOS Country List"];
        [mailViewController setToRecipients:[NSArray arrayWithObject:@"hasanal_bukhari@hotmail.com"]];
        
        [mailViewController addAttachmentData:[string dataUsingEncoding:NSUTF8StringEncoding] mimeType:@"text/plain" fileName:@"ios.txt"];
        
        [launcher presentViewController:mailViewController animated:YES completion:nil];
        
    } else {
        NSString *message = NSLocalizedString(@"Sorry, your issue can't be reported right now. This is most likely because no mail accounts are set up on your mobile device.", @"");
        [[[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles: nil] show];
    }
}


+ (void)registerForPushNotification {
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
}

+ (NSString *)replaceFirstOccuarnceFromString: (NSString *)input withOriginal:(NSString *)original AndReplacment:(NSString *)replacement
{
    NSRange rOriginal = [input rangeOfString: original];
    if (NSNotFound != rOriginal.location) {
        input = [input
                 stringByReplacingCharactersInRange: rOriginal
                 withString:                         replacement];
    }
    return input;
}

+ (NSString *)pathForName:(NSString *)name withExt:(NSString *)ext forScale:(NSInteger)scale
{
    NSString *path;
    while (scale >= 1) {
        NSString *scaleExt = scale > 1 ? [NSString stringWithFormat:@"@%ldx", (long)scale] : @"";
        NSString *realName = [name stringByAppendingString:scaleExt];
        path = [[NSBundle mainBundle] pathForResource:realName ofType:ext];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path])
            break;
        
        scale--;
    }
    
    return path;
}

+ (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    return [Common colorWithHex:x];
}

// takes 0x123456
+ (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

+ (void)fileExists:(NSString *)file_name {
    file_name = [file_name stringByReplacingOccurrencesOfString:@"/" withString:@""];
    file_name = [file_name stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *cachePath = [[NSString alloc] initWithString:[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL exist = [fileManager fileExistsAtPath:cachePath isDirectory:NO];
    if (exist) {
        NSString *file_path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@", file_name]];
        exist = [fileManager fileExistsAtPath:file_path isDirectory:NO];
        if (exist) {
            [fileManager removeItemAtPath:file_path error:nil];
        }
    }
}

+ (UIImage *)resizeImageToMaxSize:(CGFloat)max withfile:(NSString*)file_name
{
    file_name = [file_name stringByReplacingOccurrencesOfString:@"/" withString:@""];
    file_name = [file_name stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *cachePath = [[NSString alloc] initWithString:[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSString *file_path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@", file_name]];
    
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:file_path], NULL);
    if (!imageSource)
        return nil;
    
    CFDictionaryRef options = (__bridge CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                (id)kCFBooleanTrue, (id)kCGImageSourceCreateThumbnailWithTransform,
                                                (id)kCFBooleanTrue, (id)kCGImageSourceCreateThumbnailFromImageIfAbsent,
                                                (id)@(max),
                                                (id)kCGImageSourceThumbnailMaxPixelSize,
                                                nil];
    CGImageRef imgRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options);
    
    UIImage* scaled = [UIImage imageWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
    CFRelease(imageSource);
    
    return scaled;
}

+ (void)appendData:(NSData *)data toFile:(NSString *)file_name
{
    file_name = [file_name stringByReplacingOccurrencesOfString:@"/" withString:@""];
    file_name = [file_name stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *cachePath = [[NSString alloc] initWithString:[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *file_path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@", file_name]];
    
    // create file if not exist
    if(![fileManager fileExistsAtPath:file_path isDirectory:NO])
    {
        BOOL filecreationSuccess = [fileManager createFileAtPath:file_path contents:data attributes:nil];
        
        if (!filecreationSuccess)
            return;
    }
    else
    {
        NSFileHandle *fhandle = [NSFileHandle fileHandleForUpdatingAtPath:file_path];
        [fhandle seekToEndOfFile];
        [fhandle writeData:data];
        [fhandle closeFile];
    }
}

+ (void)localNotificationWithText:(NSString *)text {
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfYear|NSCalendarUnitWeekday fromDate:now];//get the required calendar units
    
    if (components.weekday>4) {
        components.weekOfYear+=1;//if already passed monday, make it next monday
    }
    components.weekday = 4;
    components.hour = 14;
    NSDate *fireDate = [calendar dateFromComponents:components];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = fireDate;
    localNotification.alertBody = text;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = localNotification.applicationIconBadgeNumber + 1;
    localNotification.repeatInterval = NSCalendarUnitMinute;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
