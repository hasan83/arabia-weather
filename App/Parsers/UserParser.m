//
//  UserParser.m
//  Yummy Wok
//
//  Created by Devloper on 3/24/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import "UserParser.h"
#import "NSDictionary+NSDictionaryCategory.h"

@implementation UserParser

@synthesize user;
@synthesize msg;

- (id)initWithJSON:(NSString *)json {
    
    if ((self = [super init])) {
        
        user = [[UserDetails alloc] initWithNoCache];
        msg = @"";
        
        @try {
            
            NSError *e = nil;
            NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                     options:kNilOptions
                                                                       error:&e];
            
            msg = [jsonDict valueForKey:@"message"];
            
            if (msg == nil)
            {
                [user setUser_id:[[jsonDict valueForKey2:@"id"] stringValue]];
                if ([[user user_id] isEqualToString:@""]) {
                    user = nil;
                } else {
                    [user setFirst_name:[jsonDict valueForKey2:@"first_name"]];
                    [user setLast_name:[jsonDict valueForKey2:@"last_name"]];
                    [user setProfile_pic:[jsonDict valueForKey2:@"pic"]];
                    [user setUsername:[jsonDict valueForKey2:@"email"]];
                    [user setGender:[jsonDict valueForKey2:@"gender"]];
                    [user setStatus:[[jsonDict valueForKey2:@"status"] stringValue]];
                    [user setShow_steps:[[jsonDict valueForKey2:@"show_steps"] stringValue]];
                }
            }
            else
            {
                user = nil;
                msg = [msg stringByReplacingOccurrencesOfString:@"<br />" withString:@"\r"];
            }
            
        } @catch (NSException *exception) {
            user = nil;
            DDLogVerbose(@"User Login ::::: Json Parsing Exception ::::: %@", [exception debugDescription]);
        }
        @finally {
            
        }
    }
    
    return self;
}

@end
