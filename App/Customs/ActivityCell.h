//
//  ActivityCell.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Activity.h"

@interface ActivityCell : UITableViewCell

@property (strong, nonatomic) Activity *activity;

@property (weak, nonatomic) IBOutlet UILabel *txtLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

- (void)setActivity:(Activity *)object;

@end
