//
//  GoController.h
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/15/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "MasterController.h"
#import "ImagesController.h"

@interface GoController : MasterController

@property (strong, nonatomic) ImagesController *controller;

@end
