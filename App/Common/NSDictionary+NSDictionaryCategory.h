//
//  NSDictionary+NSDictionaryCategory.h
//  For9a
//
//  Created by Hasan S. Al-Bukhari on 5/4/16.
//  Copyright © 2016 For9a. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionaryCategory)

- (id)valueForKey2:(NSString *)key;
- (id)objectForKey2:(NSString *)key;

@end
