//
//  Activity.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "Activity.h"

@implementation Activity

@synthesize activity_text;
@synthesize activity_text_en;
@synthesize activity_type;
@synthesize activity_color;
@synthesize activity_status;
@synthesize activity_dayInfo;
@synthesize activity_status_en;
@synthesize activity_dayInfo_en;

- (id)init
{
    if (self = [super init]) {
        activity_text = @"";
        activity_text_en = @"";
        activity_type = @"";
        activity_color = @"";
        activity_status = @"";
        activity_dayInfo = @"";
        activity_status_en = @"";
        activity_dayInfo_en = @"";
    }
    
    return  self;
}

@end
