//
//  NSDictionary+NSDictionaryCategory.m
//  For9a
//
//  Created by Hasan S. Al-Bukhari on 5/4/16.
//  Copyright © 2016 For9a. All rights reserved.
//

#import "NSDictionary+NSDictionaryCategory.h"

@implementation NSDictionary (NSDictionaryCategory)

- (id)valueForKey2:(NSString *)key {
    id value = [self valueForKey:key];
    if ([value isEqual:[NSNull null]]) {
        value = @"";
    }
    return value;
}

- (id)objectForKey2:(NSString *)key {
    id object = [self objectForKey:key];
    if ([object isEqual:[NSNull null]]) {
        object = @"";
    }
    return object;
}

@end
