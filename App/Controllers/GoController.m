//
//  GoController.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/15/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "GoController.h"

@implementation GoController

@synthesize controller;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tabBarController.title = [self.languageDetails LocalString:@"Go"];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:[self.languageDetails LocalString:@"Go"]
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(GoButtonTapped:)];
    self.tabBarController.navigationItem.rightBarButtonItem = rightButton;
}

- (void)GoButtonTapped:(UIButton*)sender
{
    [controller loadImages];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    controller = segue.destinationViewController;
    controller.concurrently = NO;
}

@end
