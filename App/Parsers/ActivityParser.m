//
//  ActivityParser.m
//  App
//
//  Created by Hasan S. Al-Bukhari on 11/14/16.
//  Copyright © 2016 Arabia Weather. All rights reserved.
//

#import "ActivityParser.h"
#import "Activity.h"

@implementation ActivityParser

@synthesize activities;

- (id)initWithJSON:(NSString *)json {
    
    if ((self = [super init])) {
        
        activities = [[NSMutableArray alloc] init];
        
        @try {
            
            NSError *e = nil;
            NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                     options:kNilOptions
                                                                       error:&e];
            
            NSDictionary *response = [jsonDict objectForKey:@"response"];
            if (response) {
                for (NSDictionary *item in response) {
                    
                    NSMutableArray *day_activities = [[NSMutableArray alloc] init];
                    NSDictionary *activity_dict = [item objectForKey:@"activity"];
                    if (activity_dict) {
                        for (NSDictionary *activity_item in activity_dict) {
                            Activity *activity = [[Activity alloc] init];
                            
                            [activity setActivity_color:[activity_item valueForKey:@"color"]];
                            [activity setActivity_dayInfo:[activity_item valueForKey:@"dayInfo"]];
                            [activity setActivity_dayInfo_en:[activity_item valueForKey:@"dayInfo_en"]];
                            [activity setActivity_status:[activity_item valueForKey:@"status"]];
                            [activity setActivity_status_en:[activity_item valueForKey:@"status_en"]];
                            [activity setActivity_text:[activity_item valueForKey:@"text"]];
                            [activity setActivity_text_en:[activity_item valueForKey:@"text_en"]];
                            [activity setActivity_type:[activity_item valueForKey:@"type"]];
                            
                            [day_activities addObject:activity];
                        }
                    }
                    
                    NSDictionary *health_dict = [item objectForKey:@"health"];
                    if (health_dict) {
                        for (NSDictionary *health_item in health_dict) {
                            Activity *activity = [[Activity alloc] init];
                            
                            [activity setActivity_color:[health_item valueForKey:@"color"]];
                            [activity setActivity_dayInfo:[health_item valueForKey:@"dayInfo"]];
                            [activity setActivity_dayInfo_en:[health_item valueForKey:@"dayInfo_en"]];
                            [activity setActivity_status:[health_item valueForKey:@"status"]];
                            [activity setActivity_status_en:[health_item valueForKey:@"status_en"]];
                            [activity setActivity_text:[health_item valueForKey:@"text"]];
                            [activity setActivity_text_en:[health_item valueForKey:@"text_en"]];
                            [activity setActivity_type:[health_item valueForKey:@"type"]];
                            
                            [day_activities addObject:activity];
                        }
                    }
                    
                    NSDictionary *sport_dict = [item objectForKey:@"sport"];
                    if (sport_dict) {
                        for (NSDictionary *sport_item in sport_dict) {
                            Activity *activity = [[Activity alloc] init];
                            
                            [activity setActivity_color:[sport_item valueForKey:@"color"]];
                            [activity setActivity_dayInfo:[sport_item valueForKey:@"dayInfo"]];
                            [activity setActivity_dayInfo_en:[sport_item valueForKey:@"dayInfo_en"]];
                            [activity setActivity_status:[sport_item valueForKey:@"status"]];
                            [activity setActivity_status_en:[sport_item valueForKey:@"status_en"]];
                            [activity setActivity_text:[sport_item valueForKey:@"text"]];
                            [activity setActivity_text_en:[sport_item valueForKey:@"text_en"]];
                            [activity setActivity_type:[sport_item valueForKey:@"type"]];
                            
                            [day_activities addObject:activity];
                        }
                    }
                    
                    [activities addObject:day_activities];
                }
            }
            
        } @catch (NSException *exception) {
            activities = nil;
            NSLog(@"Activities ::::: Json Parsing Exception ::::: %@", [exception debugDescription]);
        }
        @finally {
            
        }
    }
    
    return self;
}

@end
