//
//  UserDetails.m
//  Yummy Wok
//
//  Created by Devloper on 3/8/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import "UserDetails.h"
#import "Common.h"

@implementation UserDetails

@synthesize user_id = _user_id;
@synthesize username = _username;
@synthesize password = _password;
@synthesize first_name = _first_name;
@synthesize last_name = _last_name;
@synthesize full_name = _full_name;
@synthesize device_token = _device_token;
@synthesize employee_code = _employee_code;
@synthesize date_of_birth = _date_of_birth;
@synthesize mobile = _mobile;
@synthesize telephone = _telephone;
@synthesize department_id = _department_id;
@synthesize position_id = _position_id;
@synthesize direct_manager = _direct_manager;
@synthesize last_login = _last_login;
@synthesize department_name = _department_name;
@synthesize position_name = _position_name;
@synthesize direct_manager_full_name = _direct_manager_full_name;
@synthesize profile_pic = _profile_pic;
@synthesize code = _code;
@synthesize product_types = _product_types;
@synthesize access_token = _access_token;
@synthesize gender = _gender;
@synthesize provider = _provider;
@synthesize provider_id = _provider_id;
@synthesize phone = _phone;
@synthesize rating = _rating;
@synthesize status = _status;
@synthesize age = _age;
@synthesize facebook_url = _facebook_url;
@synthesize twitter_url = _twitter_url;
@synthesize linkedin_url = _linkedin_url;
@synthesize google_url = _google_url;
@synthesize resume = _resume;
@synthesize languages = _languages;
@synthesize show_steps = _show_steps;
@synthesize opportunity_types = _opportunity_types;
@synthesize opportunity_specialties = _opportunity_specialties;
@synthesize languages_to_learn = _languages_to_learn;
@synthesize completeness_percentage = _completeness_percentage;
@synthesize opportunities_applied_count = _opportunities_applied_count;
@synthesize opportunities_pinned_count = _opportunities_pinned_count;
@synthesize organizations_followed_count = _organizations_followed_count;
@synthesize suitable_opportunities = _suitable_opportunities;

#define USER_DETAILS @"USER_DETAILS"

// Object will "NOT" be saved in NSUserDefaults since NoCache equal no by default.
- (id)initWithNoCache
{
    if (self = [super init]) {
        [self reset];
        
        no_cache = YES; // If yes object will not be saved in NSUserDefaults.
    }
    return self;
}

// Object will be saved in NSUserDefaults since NoCache equal no by default.
- (id)init
{
    if (self = [super init]) {
        [self reset];
        
        self = [Common valueForKey:USER_DETAILS defaultValue:self];
    }
    DDLogVerbose(@"User details object initialized!");
    return  self;
}

- (void)reset {
    _user_id = @"";
    _username = @"";
    _password = @"";
    _first_name = @"";
    _last_name = @"";
    _full_name = @"";
    _device_token = @"";
    _employee_code = @"";
    _date_of_birth = @"";
    _mobile = @"";
    _telephone = @"";
    _department_id = @"";
    _position_id = @"";
    _direct_manager = @"";
    _last_login = @"";
    _department_name = @"";
    _position_name = @"";
    _direct_manager_full_name = @"";
    _profile_pic = @"";
    _code = @"";
    _product_types = @"";
    _access_token = @"";
    _gender = @"";
    _provider = @"";
    _provider_id = @"";
    _phone = @"";
    _status = @"";
    _rating = @"";
    _age = @"";
    _desc = @"";
    _facebook_url = @"";
    _twitter_url = @"";
    _linkedin_url = @"";
    _google_url = @"";
    _resume = @"";
    _languages = [[NSMutableArray alloc] init];
    _show_steps = @"";
    _opportunity_types = [[NSMutableArray alloc] init];
    _opportunity_specialties = [[NSMutableArray alloc] init];
    _languages_to_learn = [[NSMutableArray alloc] init];
    _completeness_percentage = @"";
    _opportunities_applied_count = @"";
    _opportunities_pinned_count = @"";
    _organizations_followed_count = @"";
    _suitable_opportunities = @"";
}

// set this to yes to clear login session when app. get closed.
- (void)setNoCache:(BOOL)flag
{
    no_cache = flag;
}

// Checks if user logged in.
- (BOOL)userLoggedIn
{
    return _user_id != nil && ![_user_id isEqualToString:@""];
}

- (BOOL)isFacebookUser
{
    return _provider ? ([_provider isEqualToString:@"Facebook"] ? YES : NO) : NO;
}

- (BOOL)isLinkedInUser
{
    return _provider ? ([_provider isEqualToString:@"Linkedin"] ? YES : NO) : NO;
}

- (BOOL)isSocialLogin {
    return [self isFacebookUser] || [self isLinkedInUser];
}

- (BOOL)isShow_steps {
    return [_show_steps isEqualToString:@"1"];
}

// Set user!
- (void)setUser:(UserDetails *)user
{
    [self setUser_id:user.user_id];
    [self setUsername:user.username];
    [self setPassword:user.password];
    [self setFull_name:user.full_name];
    [self setFirst_name:user.first_name];
    [self setLast_name:user.last_name];
//    [self setDevice_token:user.device_token];
    [self setEmployee_code:user.employee_code];
    [self setDate_of_birth:user.date_of_birth];
    [self setMobile:user.mobile];
    [self setTelephone:user.telephone];
    [self setDepartment_id:user.department_id];
    [self setPosition_id:user.position_id];
    [self setDirect_manager:user.direct_manager];
    [self setLast_login:user.last_login];
    [self setDepartment_name:user.department_name];
    [self setPosition_name:user.position_name];
    [self setDirect_manager_full_name:user.direct_manager_full_name];
    [self setProfile_pic:user.profile_pic];
    [self setProduct_types:user.product_types];
    [self setCode:user.code];
    [self setAccess_token:user.access_token];
    [self setGender:user.gender];
    [self setProvider:user.provider];
    [self setProvider_id:user.provider_id];
    [self setPhone:user.phone];
    [self setStatus:user.status];
    [self setRating:user.rating];
    [self setAge:user.age];
    [self setDesc:user.desc];
    [self setFacebook_url:user.facebook_url];
    [self setTwitter_url:user.twitter_url];
    [self setLinkedin_url:user.linkedin_url];
    [self setGoogle_url:user.google_url];
    [self setResume:user.resume];
    [self setLanguages:user.languages];
    [self setShow_steps:user.show_steps];
    [self setOpportunity_types:user.opportunity_types];
    [self setOpportunity_specialties:user.opportunity_specialties];
    [self setLanguages_to_learn:user.languages_to_learn];
    [self setCompleteness_percentage:user.completeness_percentage];
    [self setOpportunities_applied_count:user.opportunities_applied_count];
    [self setOpportunities_pinned_count:user.opportunities_pinned_count];
    [self setOrganizations_followed_count:user.organizations_followed_count];
    [self setSuitable_opportunities:user.suitable_opportunities];
    
    // don't set device token
    
    // check if info should be cached! saved at NSUserDefaults
    if (!no_cache)
    {
        DDLogVerbose(@"Setting user details with cach!");
        [Common setValue:self forKey:USER_DETAILS];
    }
    else
    {
        DDLogVerbose(@"Setting user details with no cach!");
    }
}

//set profile info
- (void)setUserProfile:(UserDetails *)user
{
    [self setFull_name:[NSString stringWithFormat:@"%@ %@", user.first_name, user.last_name]];
    [self setFirst_name:user.first_name];
    [self setLast_name:user.last_name];
    [self setDate_of_birth:user.date_of_birth];
    [self setProfile_pic:user.profile_pic];
    [self setGender:user.gender];
    [self setPhone:user.phone];
    [self setDesc:user.desc];
    [self setFacebook_url:user.facebook_url];
    [self setTwitter_url:user.twitter_url];
    [self setLinkedin_url:user.linkedin_url];
    [self setResume:user.resume];
    [self setLanguages:user.languages];
    [self setOpportunity_types:user.opportunity_types];
    [self setOpportunity_specialties:user.opportunity_specialties];
    [self setLanguages_to_learn:user.languages_to_learn];
    [self setCompleteness_percentage:user.completeness_percentage];
    [self setOpportunities_applied_count:user.opportunities_applied_count];
    [self setOpportunities_pinned_count:user.opportunities_pinned_count];
    [self setOrganizations_followed_count:user.organizations_followed_count];
    [self setSuitable_opportunities:user.suitable_opportunities];
    
    // check if info should be cached! saved at NSUserDefaults
    if (!no_cache)
    {
        DDLogVerbose(@"Setting user details with cach!");
        [Common setValue:self forKey:USER_DETAILS];
    }
    else
    {
        DDLogVerbose(@"Setting user details with no cach!");
    }
}

- (void)setShow_steps:(NSString *)show_steps {
    _show_steps = show_steps ? show_steps : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Show Steps: %@", show_steps);
}

- (void)setOpportunity_types:(NSMutableArray *)opportunity_types {
    _opportunity_types = opportunity_types ? opportunity_types : [[NSMutableArray alloc] init];
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Opportunity Types: %@", opportunity_types);
}

- (void)setOpportunity_specialties:(NSMutableArray *)opportunity_specialties {
    _opportunity_specialties = opportunity_specialties ? opportunity_specialties : [[NSMutableArray alloc] init];
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Opportunity Specialties: %@", opportunity_specialties);
}

- (void)setLanguages_to_learn:(NSMutableArray *)languages_to_learn {
    _languages_to_learn = languages_to_learn ? languages_to_learn : [[NSMutableArray alloc] init];
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Languages to Learn: %@", languages_to_learn);
}

- (void)setCompleteness_percentage:(NSString *)completeness_percentage {
    _completeness_percentage = completeness_percentage ? completeness_percentage : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Completeness Percentage: %@", completeness_percentage);
}

- (void)setSuitable_opportunities:(NSString *)suitable_opportunities {
    _suitable_opportunities = suitable_opportunities ? suitable_opportunities : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Suitable Opportunities: %@", suitable_opportunities);
}

- (void)setOpportunities_applied_count:(NSString *)opportunities_applied_count {
    _opportunities_applied_count = opportunities_applied_count ? opportunities_applied_count : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Opportunities Applied Count: %@", opportunities_applied_count);
}

- (void)setOpportunities_pinned_count:(NSString *)opportunities_pinned_count {
    _opportunities_pinned_count = opportunities_pinned_count ? opportunities_pinned_count : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Opportunities Pinned Count: %@", opportunities_pinned_count);
}

- (void)setOrganizations_followed_count:(NSString *)organizations_followed_count {
    _organizations_followed_count = organizations_followed_count ? organizations_followed_count : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Organizations Followed Count: %@", organizations_followed_count);
}

- (void)setResume:(NSString *)resume {
    _resume = resume ? resume : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Resume: %@", resume);
}

- (void)setLanguages:(NSMutableArray *)languages {
    _languages = languages ? languages : [[NSMutableArray alloc] init];
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Languages: %@", languages);
}

- (void)setFacebook_url:(NSString *)facebook_url {
    _facebook_url = facebook_url ? facebook_url : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Facebook Url: %@", facebook_url);
}

- (void)setTwitter_url:(NSString *)twitter_url {
    _twitter_url = twitter_url ? twitter_url : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Twitter Url: %@", twitter_url);
}

- (void)setLinkedin_url:(NSString *)linkedin_url {
    _linkedin_url = linkedin_url ? linkedin_url : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"LinkedIn Url: %@", linkedin_url);
}

- (void)setGoogle_url:(NSString *)google_url {
    _google_url = google_url ? google_url : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Google Url: %@", google_url);
}

- (void)setDesc:(NSString *)desc {
    _desc = desc ? desc : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Desc: %@", desc);
}

- (void)setAge:(NSString *)age
{
    _age = age ? age : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Age: %@", age);
}

- (void)setProvider:(NSString *)provider
{
    _provider = provider ? provider : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Provider: %@", provider);
}

- (void)setProvider_id:(NSString *)provider_id
{
    _provider_id = provider_id ? provider_id : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Provider Id: %@", provider_id);
}

- (void)setGender:(NSString *)gender
{
    _gender = gender ? gender : @"";
    _gender = [_gender isEqualToString:@"male"] ? @"m" : _gender;
    _gender = [_gender isEqualToString:@"female"] ? @"f" : _gender;
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Gender: %@", gender);
}

- (void)setProduct_types:(NSString *)product_types {
    _product_types = product_types ? product_types : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"User Selected Types: %@", product_types);
}

- (void)setProfile_pic:(NSString *)profile_pic
{
    _profile_pic = profile_pic ? profile_pic : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Profile pic: %@", profile_pic);
}

- (void)setDirect_manager_full_name:(NSString *)direct_manager_full_name
{
    _direct_manager_full_name = direct_manager_full_name ? direct_manager_full_name : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Direct manager full name: %@", direct_manager_full_name);
}

- (void)setPosition_name:(NSString *)position_name
{
    _position_name = position_name ? position_name : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Position name: %@", position_name);
}

- (void)setDepartment_name:(NSString *)department_name
{
    _department_name = department_name ? department_name : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Department name: %@", department_name);
}

- (void)setLast_login:(NSString *)last_login
{
    _last_login = last_login ? last_login : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Last login: %@", last_login);
}

- (void)setDirect_manager:(NSString *)direct_manager
{
    _direct_manager = direct_manager ? direct_manager : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Direct manager: %@", direct_manager);
}

- (void)setPosition_id:(NSString *)position_id
{
    _position_id = position_id ? position_id : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Position id: %@", position_id);
}

- (void)setDepartment_id:(NSString *)department_id
{
    _department_id = department_id ? department_id : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Department id: %@", department_id);
}

- (void)setTelephone:(NSString *)telephone
{
    _telephone = telephone ? telephone : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Telephone: %@", telephone);
}

- (void)setMobile:(NSString *)mobile
{
    _mobile = mobile ? mobile : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Mobile: %@", mobile);
}

- (void)setPhone:(NSString *)phone
{
    _phone = phone ? phone : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Phone: %@", phone);
}

- (void)setStatus:(NSString *)status
{
    _status = status ? status : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Status: %@", status);
}

- (void)setRating:(NSString *)rating
{
    _rating = rating ? rating : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Rating: %@", rating);
}

- (void)setDate_of_birth:(NSString *)date_of_birth
{
    _date_of_birth = date_of_birth ? date_of_birth : @"";
    
    if (![_date_of_birth isEqualToString:@""]) {
        NSDate *date = [Common stringToDate:[Common timeStampToDate:date_of_birth]];
        NSDate* now = [NSDate date];
        NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                           components:NSCalendarUnitYear
                                           fromDate:date
                                           toDate:now
                                           options:0];
        _age = [NSString stringWithFormat:@"%ld", (long)[ageComponents year]];
    }
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Date of birth: %@", _date_of_birth);
}

- (void)setEmployee_code:(NSString *)employee_code
{
    _employee_code = employee_code ? employee_code : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Employee code: %@", _employee_code);
}

- (void)setUser_id:(NSString *)user_id
{
    _user_id = user_id ? user_id : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"User_id: %@", _user_id);
}

- (void)setUsername:(NSString *)username
{
    _username = username ? username : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Username: %@", _username);
}

- (void)setPassword:(NSString *)password
{
    _password = password ? password : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Password: %@", @"****");
}

- (void)setFull_name:(NSString *)full_name
{
    _full_name = full_name ? full_name : @"";
    _first_name = full_name ? full_name : @"";
    _last_name = full_name ? full_name : @"";
    
    if (_first_name != nil)
    {
        NSRange range = [_first_name rangeOfString:@" "];
        _first_name = range.length > 0 ? [_first_name substringToIndex:range.location] : _first_name;
    }
    
    if (_last_name != nil)
    {
        NSRange range = [_last_name rangeOfString:@" " options:NSBackwardsSearch];
        _last_name = range.length > 0 ? [_last_name substringFromIndex:range.location+1] : _last_name;
    }
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Fullname: %@", _full_name);
}

- (void)setFirst_name:(NSString *)first_name
{
    _first_name = first_name ? first_name : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Name: %@", _first_name);
}

- (void)setLast_name:(NSString *)last_name
{
    _last_name = last_name ? last_name : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Name: %@", _last_name);
}

// can be used as an id incase the user is not logged in. not other use.
- (void)setDevice_token:(NSString *)device_token
{
    _device_token = device_token ? device_token : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Device Token: %@", _device_token);
}

- (void)setCode:(NSString *)code {
    _code = code ? code : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"API Code: %@", _code);
}

- (void)setAccess_token:(NSString *)access_token {
    _access_token = access_token ? access_token : @"";
    
    if (!no_cache)
        [Common setValue:self forKey:USER_DETAILS];
    
    //DDLogVerbose(@"Access Token: %@", _access_token);
}

// will be used to store and fetch the object in NSUserDefaults
- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_user_id forKey:@"1"];
    [aCoder encodeObject:_username forKey:@"2"];
    [aCoder encodeObject:_password forKey:@"3"];
    [aCoder encodeObject:_first_name forKey:@"4"];
    [aCoder encodeObject:_full_name forKey:@"5"];
    [aCoder encodeObject:_device_token forKey:@"6"];
    [aCoder encodeObject:_employee_code forKey:@"7"];
    [aCoder encodeObject:_date_of_birth forKey:@"8"];
    [aCoder encodeObject:_mobile forKey:@"10"];
    [aCoder encodeObject:_telephone forKey:@"11"];
    [aCoder encodeObject:_department_id forKey:@"12"];
    [aCoder encodeObject:_position_id forKey:@"13"];
    [aCoder encodeObject:_direct_manager forKey:@"14"];
    [aCoder encodeObject:_last_login forKey:@"15"];
    [aCoder encodeObject:_department_name forKey:@"16"];
    [aCoder encodeObject:_position_name forKey:@"17"];
    [aCoder encodeObject:_direct_manager_full_name forKey:@"18"];
    [aCoder encodeObject:_profile_pic forKey:@"19"];
    [aCoder encodeObject:_code forKey:@"20"];
    [aCoder encodeObject:_product_types forKey:@"21"];
    [aCoder encodeObject:_access_token forKey:@"22"];
    [aCoder encodeObject:_gender forKey:@"23"];
    [aCoder encodeObject:_provider forKey:@"24"];
    [aCoder encodeObject:_provider_id forKey:@"25"];
    [aCoder encodeObject:_last_name forKey:@"26"];
    [aCoder encodeObject:_phone forKey:@"27"];
    [aCoder encodeObject:_status forKey:@"28"];
    [aCoder encodeObject:_rating forKey:@"29"];
    [aCoder encodeObject:_age forKey:@"30"];
    [aCoder encodeObject:_desc forKey:@"31"];
    [aCoder encodeObject:_facebook_url forKey:@"34"];
    [aCoder encodeObject:_twitter_url forKey:@"35"];
    [aCoder encodeObject:_linkedin_url forKey:@"36"];
    [aCoder encodeObject:_google_url forKey:@"37"];
    [aCoder encodeObject:_resume forKey:@"40"];
    [aCoder encodeObject:_languages forKey:@"41"];
    [aCoder encodeObject:_show_steps forKey:@"42"];
    [aCoder encodeObject:_opportunity_types forKey:@"43"];
    [aCoder encodeObject:_opportunity_specialties forKey:@"44"];
    [aCoder encodeObject:_languages_to_learn forKey:@"45"];
    [aCoder encodeObject:_completeness_percentage forKey:@"46"];
    [aCoder encodeObject:_opportunities_applied_count forKey:@"47"];
    [aCoder encodeObject:_opportunities_pinned_count forKey:@"48"];
    [aCoder encodeObject:_organizations_followed_count forKey:@"49"];
    [aCoder encodeObject:_suitable_opportunities forKey:@"50"];
    
//    DDLogVerbose(@"User details encoded");
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        _user_id = [aDecoder decodeObjectForKey:@"1"];
        _username = [aDecoder decodeObjectForKey:@"2"];
        _password = [aDecoder decodeObjectForKey:@"3"];
        _first_name = [aDecoder decodeObjectForKey:@"4"];
        _full_name = [aDecoder decodeObjectForKey:@"5"];
        _device_token = [aDecoder decodeObjectForKey:@"6"];
        _employee_code = [aDecoder decodeObjectForKey:@"7"];
        _date_of_birth = [aDecoder decodeObjectForKey:@"8"];
        _mobile = [aDecoder decodeObjectForKey:@"10"];
        _telephone = [aDecoder decodeObjectForKey:@"11"];
        _department_id = [aDecoder decodeObjectForKey:@"12"];
        _position_id = [aDecoder decodeObjectForKey:@"13"];
        _direct_manager = [aDecoder decodeObjectForKey:@"14"];
        _last_login = [aDecoder decodeObjectForKey:@"15"];
        _department_name = [aDecoder decodeObjectForKey:@"16"];
        _position_name = [aDecoder decodeObjectForKey:@"17"];
        _direct_manager_full_name = [aDecoder decodeObjectForKey:@"18"];
        _profile_pic = [aDecoder decodeObjectForKey:@"19"];
        _code = [aDecoder decodeObjectForKey:@"20"];
        _product_types = [aDecoder decodeObjectForKey:@"21"];
        _access_token = [aDecoder decodeObjectForKey:@"22"];
        _gender = [aDecoder decodeObjectForKey:@"23"];
        _provider = [aDecoder decodeObjectForKey:@"24"];
        _provider_id = [aDecoder decodeObjectForKey:@"25"];
        _last_name = [aDecoder decodeObjectForKey:@"26"];
        _phone = [aDecoder decodeObjectForKey:@"27"];
        _status = [aDecoder decodeObjectForKey:@"28"];
        _rating = [aDecoder decodeObjectForKey:@"29"];
        _age = [aDecoder decodeObjectForKey:@"30"];
        _desc = [aDecoder decodeObjectForKey:@"31"];
        _facebook_url = [aDecoder decodeObjectForKey:@"34"];
        _twitter_url = [aDecoder decodeObjectForKey:@"35"];
        _linkedin_url = [aDecoder decodeObjectForKey:@"36"];
        _google_url = [aDecoder decodeObjectForKey:@"37"];
        _resume = [aDecoder decodeObjectForKey:@"40"];
        _languages = [aDecoder decodeObjectForKey:@"41"];
        _show_steps = [aDecoder decodeObjectForKey:@"42"];
        _opportunity_types = [aDecoder decodeObjectForKey:@"43"];
        _opportunity_specialties = [aDecoder decodeObjectForKey:@"44"];
        _languages_to_learn = [aDecoder decodeObjectForKey:@"45"];
        _completeness_percentage = [aDecoder decodeObjectForKey:@"46"];
        _opportunities_applied_count = [aDecoder decodeObjectForKey:@"47"];
        _opportunities_pinned_count = [aDecoder decodeObjectForKey:@"48"];
        _organizations_followed_count = [aDecoder decodeObjectForKey:@"49"];
        _suitable_opportunities = [aDecoder decodeObjectForKey:@"50"];
        
        if (_user_id == nil)
            _user_id = @"";
        if (_username == nil)
            _username = @"";
        if (_password == nil)
            _password = @"";
        if (_first_name == nil)
            _first_name = @"";
        if (_last_name == nil)
            _last_name = @"";
        if (_full_name == nil)
            _full_name = @"";
        if (_device_token == nil)
            _device_token = @"";
        if (_employee_code == nil)
            _employee_code = @"";
        if (_date_of_birth == nil)
            _date_of_birth = @"";
        if (_mobile == nil)
            _mobile = @"";
        if (_telephone == nil)
            _telephone = @"";
        if (_department_id == nil)
            _department_id = @"";
        if (_position_id == nil)
            _position_id = @"";
        if (_direct_manager == nil)
            _direct_manager = @"";
        if (_last_login == nil)
            _last_login = @"";
        if (_department_name == nil)
            _department_name = @"";
        if (_position_name == nil)
            _position_name = @"";
        if (_direct_manager_full_name == nil)
            _direct_manager_full_name = @"";
        if (_profile_pic == nil)
            _profile_pic = @"";
        if (_code == nil)
            _code = @"";
        if (_product_types == nil)
            _product_types = @"";
        if (_access_token == nil)
            _access_token = @"";
        if (_gender == nil)
            _gender = @"";
        if (_provider == nil)
            _provider = @"";
        if (_provider_id == nil)
            _provider_id = @"";
        if (_phone == nil)
            _phone = @"";
        if (_status == nil)
            _status = @"";
        if (_rating == nil)
            _rating = @"";
        if (_age == nil)
            _age = @"";
        if (_desc == nil)
            _desc = @"";
        if (_facebook_url == nil)
            _facebook_url = @"";
        if (_twitter_url == nil)
            _twitter_url = @"";
        if (_linkedin_url == nil)
            _linkedin_url = @"";
        if (_google_url == nil)
            _google_url = @"";
        if (_resume == nil)
            _resume = @"";
        if (_languages == nil)
            _languages = [[NSMutableArray alloc] init];
        if (_show_steps == nil)
            _show_steps = @"";
        if (_opportunity_types == nil)
            _opportunity_types = [[NSMutableArray alloc] init];
        if (_opportunity_specialties == nil)
            _opportunity_specialties = [[NSMutableArray alloc] init];
        if (_languages_to_learn == nil)
            _languages_to_learn = [[NSMutableArray alloc] init];
        if (_completeness_percentage == nil)
            _completeness_percentage = @"";
        if (_opportunities_applied_count == nil)
            _opportunities_applied_count = @"";
        if (_opportunities_pinned_count == nil)
            _opportunities_pinned_count = @"";
        if (_organizations_followed_count == nil)
            _organizations_followed_count = @"";
        if (_suitable_opportunities == nil)
            _suitable_opportunities = @"";
        
//        DDLogVerbose(@"User details decoded");
    }
    return self;
}

@end
