//
//  StylingDetails.m
//  Yummy Wok
//
//  Created by Devloper on 3/12/15.
//  Copyright (c) 2015 Devloper. All rights reserved.
//

#import "StylingDetails.h"

@implementation StylingDetails

@synthesize themeColor;
@synthesize themeSecondaryColor;
@synthesize themeKaKiColor;
@synthesize themeBlueColor;
@synthesize themeGrayColor;
@synthesize themeBackColor;

- (id)init
{
    if (self = [super init]) {
        
        themeColor = [UIColor colorWithRed:235/255.0 green:117/255.0 blue:29/255.0 alpha:1.0f];
        themeSecondaryColor = [UIColor colorWithRed:12/255.0 green:93/255.0 blue:107/255.0 alpha:1.0f];
        themeKaKiColor = [UIColor colorWithRed:225/255.0 green:97/255.0 blue:16/255.0 alpha:1.0f];
        
        themeGrayColor = [UIColor colorWithRed:220/255.0 green:221/255.0 blue:222/255.0 alpha:1.0f];
        themeBackColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1.0f];
        themeBlueColor = [UIColor colorWithRed:13/255.0 green:114/255.0 blue:254/255.0 alpha:1.0f];
        
        // To use custom fonts. Add the ttf or otf file to the app. and list it in the plist.
        // then, un-comment those lines to get font name to be used in code.
//        for (NSString* family in [UIFont familyNames])
//        {
//            NSLog(@"%@", family);
//            
//            for (NSString* name in [UIFont fontNamesForFamilyName: family])
//            {
//                NSLog(@"  %@", name);
//            }
//        }
    }
    return  self;
}

@end
